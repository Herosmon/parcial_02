<?php

class Edition extends ModeloGenerico{
    protected $idEdition;
    protected $name;
    protected $year;
    protected $startDate;
    protected $endData;
    protected $internationalCollaboration;
    protected $numberOfKeynotes;
	

    public function __construct($propiedades=null){
        parent:: __construct("Edition",Edition::class. $propiedades);
    }


   

	function getIdEdition() { 
 		return $this->idEdition; 
	} 

	function setIdEdition($idEdition) {  
		$this->idEdition = $idEdition; 
	} 

	function getName() { 
 		return $this->name; 
	} 

	function setName($name) {  
		$this->name = $name; 
	} 

	function getYear() { 
 		return $this->year; 
	} 

	function setYear($year) {  
		$this->year = $year; 
	} 

	function getStartDate() { 
 		return $this->startDate; 
	} 

	function setStartDate($startDate) {  
		$this->startDate = $startDate; 
	} 

	function getEndData() { 
 		return $this->endData; 
	} 

	function setEndData($endData) {  
		$this->endData = $endData; 
	} 

	function getInternationalCollaboration() { 
 		return $this->internationalCollaboration; 
	} 

	function setInternationalCollaboration($internationalCollaboration) {  
		$this->internationalCollaboration = $internationalCollaboration; 
	} 

	function getNumberOfKeynotes() { 
 		return $this->numberOfKeynotes; 
	} 

	function setNumberOfKeynotes($numberOfKeynotes) {  
		$this->numberOfKeynotes = $numberOfKeynotes; 
	} 
}