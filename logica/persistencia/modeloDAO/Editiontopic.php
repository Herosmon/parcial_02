<?php


class Editiontopic extends ModeloGenerico{
    protected $idEditionTopic;
    protected $accepted;
    protected $rejeacted;
    protected $edition_idEdition;
    protected $topic_idTopic;


    public function __construct($propiedades=null){
        parent:: __construct("EditionTopic",Editiontopic::class. $propiedades);
    }
    

	function getIdEditionTopic() { 
 		return $this->idEditionTopic; 
	} 

	function setIdEditionTopic($idEditionTopic) {  
		$this->idEditionTopic = $idEditionTopic; 
	} 

	function getAccepted() { 
 		return $this->accepted; 
	} 

	function setAccepted($accepted) {  
		$this->accepted = $accepted; 
	} 


	function getRejeacted() { 
        return $this->rejeacted; 
   } 

   function setRejeacted($rejeacted) {  
       $this->rejeacted = $rejeacted; 
   } 

   
   function getEditionidEdition() { 
       return $this->editionidEdition; 
	} 
    
	function setEditionidEdition($editionidEdition) {  
        $this->editionidEdition = $editionidEdition; 
	} 
    
	function getTopicidTopic() { 
        return $this->topicidTopic; 
	} 
    
	function setTopicidTopic($topicidTopic) {  
        $this->topicidTopic = $topicidTopic; 
	} 
    
}