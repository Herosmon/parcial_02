<?php

class Topic extends ModeloGenerico{
    protected $idTopic;
    protected $name;
	

    public function __construct($propiedades=null){
        parent:: __construct("Topic",Topic::class. $propiedades);
    }

	function getIdTopic() { 
 		return $this->idTopic; 
	} 

	function setIdTopic($idTopic) {  
		$this->idTopic = $idTopic; 
	} 

	function getName() { 
 		return $this->name; 
	} 

	function setName($name) {  
		$this->name = $name; 
	} 

}